{-
grep :: String -> String -> String -> [String]
grep word flags fileName = [word]
-}


grep regex []  = False
--grep (r:regex) line = 

-------------- WORD -> "abc" "cba" "xxxxxxxx"  ---------------

wordR regex [] = False
wordR regex (l:line) 
			| (loopWordR regex (l:line)) == True = True
			| otherwise = wordR regex line

loopWordR [] line = True
loopWordR regex [] = False
loopWordR (r:regex) (l:line)
			| r == '[' = False
			| r == l = loopWordR regex line
			| otherwise = False
					
-------- regex == [a-zA-z...] -----------

generateListLetters (r:regex)
			| r == ']' = []
			| r == '[' = generateListLetters regex
			| otherwise = (generateLetters r (regex !! 1)) ++ (generateListLetters (removeFirst regex 2))

generateLetters l1 l2 = foldr (\x y-> [x] ++ y) [] [l1..l2]

substring [] i = []
substring (s:str) i 
		| i > 0 = [s] ++ (substring str (i-1))
		| otherwise = []

removeFirst [] i = []
removeFirst (s:str) i 
			| i > 0 = removeFirst str (i-1)
			| otherwise = [s] ++ str

containsElem [] el = False
containsElem (l:list) el 
			| l == el = True
			| otherwise = containsElem list el

starSign [] = False
starSign [r] = False
starSign (r1:r2:regex)
		| r1 == ']' && r2 == '*' = True
		| otherwise = starSign (r2:regex) 


questionSign [] = False
questionSign [r] = False
questionSign (r1:r2:regex)
		| r1 == ']' && r2 == '?' = True
		| otherwise = questionSign (r2:regex) 


plusSign [] = False
plusSign [r] = False
plusSign (r1:r2:regex)
		| r1 == ']' && r2 == '+' = True
		| otherwise = plusSign (r2:regex) 
