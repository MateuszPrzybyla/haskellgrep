import System.IO;

{- 
	"abc" <-> Plain "abc"
	"a[bc]" <-> Concat (Plain "a") (AnyGroup "bc")
	"a*bc" <-> Concat (Star (Plain "a")) (Plain "bc")
	"ab(cd)?" <-> Concat (Plain "ab") (Optional (Group (Plain "cd")))
	"ab*(cd)+" <-> Concat (Concat (Plain "a") (Star (Plain "b"))) (Plus (Group (Plain "cd")))
-}
data FullRegex = Floating Regex -- "a[lm]a"
					 | Start Regex -- "^a[lm]a"
					 | End Regex -- "a[lm]a$"
					 | StartEnd Regex deriving Show -- "^a[lm]a$"
           
data Regex = DotChar
           | Plain String
           | Concat Regex Regex
           | Group Regex -- (abc) 
           | AnyGroup [Char] -- [abc]
           | Optional Regex -- abc?
           | Star Regex -- abc*
           | Plus Regex deriving Show -- abc+

  
{-         
instance Show Regex where
	show (Plain a) = a
	show (Concat first second) = (show first) ++ (show second)
	show (Group r) = "(" ++ (show r) ++ ")"
	show (AnyGroup g) = "[" ++ g ++ "]"
	show (Optional r) = (show r) ++ "?"
	show (Star r) = (show r) ++ "*"
	show (Plus r) = (show r) ++ "+"
	show DotChar = "."
-}

isRegexAlwaysEmpty :: Regex -> Bool
isRegexAlwaysEmpty (Plain str) = str == ""
isRegexAlwaysEmpty (Concat reg1 reg2) = (isRegexAlwaysEmpty reg1) && (isRegexAlwaysEmpty reg2)
isRegexAlwaysEmpty (Group reg) = isRegexAlwaysEmpty reg
isRegexAlwaysEmpty (AnyGroup chars) = chars == ""
isRegexAlwaysEmpty (Optional reg) = isRegexAlwaysEmpty reg
isRegexAlwaysEmpty (Star reg) = isRegexAlwaysEmpty reg
isRegexAlwaysEmpty (Plus reg) = isRegexAlwaysEmpty reg
isRegexAlwaysEmpty DotChar = False


transformFullRegex :: String -> FullRegex
transformFullRegex [] = Floating (Plain "")
transformFullRegex reg 
	| (head reg) == '^' && (head (reverse reg)) == '$' = StartEnd (transformRegex (substring reg 1 (length reg - 1)))
	| (head reg) == '^' = Start (transformRegex (suffix 1 reg))
	| (head (reverse reg)) == '$' = End (transformRegex (prefix (length reg - 1) reg))
	| otherwise = Floating (transformRegex reg)


---------regex transform ------------------- (dorobic zeby tez parsowalo kropke na DotChar)
transformRegex :: String -> Regex
transformRegex [] = Plain ""
transformRegex (r:regex) 
--------------------------------------ANY GROUP --------------------------------
			| r == '[' && (starSign regex ']') = Concat (Star (AnyGroup (generateListLetters regex))) (transformRegex (removeProcessedRegex regex ']' 2))
			| r == '[' && (optionalSign regex ']') = Concat (Optional (AnyGroup (generateListLetters regex))) (transformRegex (removeProcessedRegex regex ']' 2))
			| r == '[' && (plusSign regex ']') = Concat (Plus (AnyGroup (generateListLetters regex))) (transformRegex (removeProcessedRegex regex ']' 2))
			| r == '[' = Concat (AnyGroup (generateListLetters regex)) (transformRegex (removeProcessedRegex regex ']' 1))
---------------------------------------GROUP -----------------------------------
			| r == '(' && (starSign regex ')') = Concat (Star (Group (transformRegex (substringToChar regex ')')))) (transformRegex (removeProcessedRegex regex ')' 2))
			| r == '(' && (optionalSign regex ')') = Concat (Optional (Group (transformRegex (substringToChar regex ')')))) (transformRegex (removeProcessedRegex regex ')' 2))
			| r == '(' && (plusSign regex ')') = Concat (Plus (Group (transformRegex (substringToChar regex ')')))) (transformRegex (removeProcessedRegex regex ')' 2))
			| r == '(' = Concat (Group (transformRegex (substringToChar regex ')'))) (transformRegex (removeProcessedRegex regex ')' 1))
---------------------------------------DOT -----------------------------------
			| r == '.' && (starSign (r:regex) '.') = Concat (Star DotChar) (transformRegex (rmFirst regex 1))
			| r == '.' && (optionalSign (r:regex) '.') = Concat (Optional DotChar) (transformRegex (rmFirst regex 1))
			| r == '.' && (plusSign (r:regex) '.') = Concat (Plus DotChar) (transformRegex (rmFirst regex 1))
			| r == '.' = Concat DotChar (transformRegex regex)
---------------------------------------PLAIN -----------------------------------
			| r == '\\' = Concat (Plain (prefix 1 regex)) (transformRegex (rmFirst regex 1))
			| (starSign (r:regex) r) = Concat (Star (Plain [r])) (transformRegex (rmFirst regex 1))
			| (optionalSign (r:regex) r) = Concat (Optional (Plain [r])) (transformRegex (rmFirst regex 1))
			| (plusSign (r:regex) r) = Concat (Plus (Plain [r])) (transformRegex (rmFirst regex 1))
			| otherwise = Concat (Plain (getPlainRegex (r:regex))) (transformRegex (rmFirst (r:regex) (length (getPlainRegex (r:regex)))))
			
starSign [] c = False
starSign [r] c = False
starSign (r1:r2:regex) c
		| r1 == c && r2 == '*' = True
		| otherwise = starSign (r2:regex) c


optionalSign [] c = False
optionalSign [r] c = False
optionalSign (r1:r2:regex) c
		| r1 == c && r2 == '?' = True
		| otherwise = optionalSign (r2:regex) c


plusSign [] c = False
plusSign [r] c = False
plusSign (r1:r2:regex) c
		| r1 == c && r2 == '+' = True
		| otherwise = plusSign (r2:regex) c

generateListLetters (r:regex)
			| r == ']' = []
			| r == '[' = generateListLetters regex
			| length (substringToChar regex ']') > 1 && (regex !! 0) == '-' = (generateLetters r (regex !! 1)) ++ (generateListLetters (rmFirst regex 2))
			| otherwise = [r] ++ (generateListLetters regex)

generateLetters l1 l2 = foldr (\x y-> [x] ++ y) [] [l1..l2]

getPlainRegex [] = []
getPlainRegex [c]
	| all (/=c) "\\.*?+[(" = [c]
	| otherwise = []
getPlainRegex (c1:c2:chars) 
	| any (==c2) "*?+" = []
	| any (==c2) "\\.[(" = [c1]
	| otherwise = c1:(getPlainRegex (c2:chars))


removeProcessedRegex regex c addLen = rmFirst regex ((length (substringToChar regex c) + addLen))

---------STRING FUNCTIONS----------------
substring str from to = drop from (take to str)
prefix to str = take to str
suffix from str = drop from str


substringToChar [] c = []
substringToChar [s] c = if s == c then [] else [s]
substringToChar (s1:s2:str) c 
			| s1 == '\\' = [s1] ++ [s2] ++ (substringToChar str c)
			| s1 == c = []
			| otherwise = [s1] ++ (substringToChar (s2:str) c)


rmFirst [] i = []
rmFirst str i = drop i str
			
			
-------- LINE MATCHING ---------------
matchesFull :: String -> FullRegex -> Bool
matchesFull expression (StartEnd regex) = matches expression regex
matchesFull expression (Start regex) = any ((flip matches) regex) [(take n expression) | n <- [0..(length expression)]]
matchesFull expression (End regex) = any ((flip matches) regex) [(drop n expression) | n <- [0..(length expression)]]
matchesFull expression (Floating regex) = any ((flip matches) regex) [drop from (take to expression) | from <- [0..(length expression)], to <- [(from+1)..(length expression)]]

matches :: String -> Regex -> Bool
matches expression DotChar = (length expression) == 1
matches expression (Plain regexText) = expression == regexText
matches expression (Concat first second) = matchConcat "" expression first second where
	matchConcat expFirst expSecond first second 
		| (matches expFirst first) && (matches expSecond second) = True
		| expSecond == [] = False
		| otherwise = matchConcat (expFirst++[(head expSecond)]) (tail expSecond) first second
matches expression (Group regex) = matches expression regex
matches expression (AnyGroup group) 
	| (length expression) /= 1 = False
	| otherwise = any (==(head expression)) group
matches "" (Optional regex) = True
matches expression (Optional regex) = matches expression regex 
matches "" (Star regex) = True
matches expression (Star regex) = any (tryMatchStar expression regex) [1..(length expression)]
matches expression (Plus regex)
	| expression == "" = isRegexAlwaysEmpty regex
	| otherwise = matches expression (Star regex)
tryMatchStar expression regex headMatch 
	| matches (take headMatch expression) regex = matches (drop headMatch expression) (Star regex)
	| otherwise = False
 
matchesStr :: String -> String -> Bool
matchesStr expression regexStr = matchesFull expression (transformFullRegex regexStr) 


removeIgnoreCaseFlag flags = foldl (\x y -> if y /= 'i' then x ++ [y] else x) [] flags

isIgnoreCaseFlag flags = any (== 'i') flags

upperCase str = map (upperCaseChar) str

upperCaseChar char 
		| any (== char) ['a'..'z'] = foldl (\x y -> if (fst y) == char then (snd y) else x) ' ' (zip ['a'..'z'] ['A'..'Z'])
		| otherwise = char


grepAllFile :: Handle -> FullRegex -> [Char] -> Int -> Int -> IO Int
grepAllFile fHandle regex flags lineNum lineOffset = do
  eof <- hIsEOF fHandle
  if eof then return 0
  else
    do
	line <- hGetLine fHandle
	if isIgnoreCaseFlag flags then
		do
		lineResult <- (handleFlags (removeIgnoreCaseFlag flags) (matchesFull (upperCase line) regex) line lineNum lineOffset)		
		putStr (fst lineResult)
		next <- (grepAllFile fHandle regex flags (lineNum + 1) (lineOffset + (length line) + 1))
		return ((snd lineResult) + next)
	else
		do		
		lineResult <- (handleFlags flags (matchesFull line regex) line lineNum lineOffset)
		putStr (fst lineResult)
		next <- (grepAllFile fHandle regex flags (lineNum + 1) (lineOffset + (length line) + 1))
		return ((snd lineResult) + next)

handleFlags :: [Char] -> Bool -> String -> Int -> Int -> IO (String, Int)
handleFlags flags matches line lineNum lineOffset
  | any (=='c') flags = return ([], if any (=='v') flags && (not matches) || all (/='v') flags && matches then 1 else 0)
  | any (=='v') flags && (not matches) = return ((foldl (handleFlag lineNum lineOffset) line flags) ++ "\n", 1)
  | all (/='v') flags && matches = return ((foldl (handleFlag lineNum lineOffset) line flags) ++ "\n", 1)
  | otherwise = return ([],0)
  
handleFlag :: Int -> Int -> String -> Char -> String
handleFlag lineNum lineOffset line flag
  | flag == 'b' = (show lineOffset) ++ ':' : line
  | flag == 'n' = (show lineNum) ++ ':' : line
  | otherwise = line

grep :: String -> String -> String -> IO ()
grep flags pattern fName = do
  handle <- openFile fName ReadMode
  if isIgnoreCaseFlag flags then 
	do 
	count <- grepAllFile handle (transformFullRegex (upperCase pattern)) flags 0 0
	if any (=='c') flags then putStrLn (show count) else putStr ""
  else
	do
	count <- grepAllFile handle (transformFullRegex pattern) flags 0 0
	if any (=='c') flags then putStrLn (show count) else putStr ""
  hClose handle
